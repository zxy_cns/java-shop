package com.example.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.example.service.HszMoneyService;
import com.example.service.UserService;
import com.example.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @description: 用户管理
 * @author: xxx
 * @create: 2021/7/19 09:30
 */
@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserService service;
    @Autowired
    private HszMoneyService hszMoneyService;
    @Autowired
    private RedisUtil redisUtil;
    /**
     * @description: 分页
     * @param json
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/7/19 09:30
     */
    @RequestMapping("getPage")
    public Result getPage(@RequestBody String json) {

        JSONObject jsonObject = JSON.parseObject(json);
//        jsonObject.put("type","p");

        jsonObject.put("state","NORMAL");
        Page.getPage(jsonObject);
        List<Map<String, Object>> list = service.getListNew(jsonObject);
        int total = service.getCountNew(jsonObject);

        return ResultUtil.successPage(list,total);
    }
    /**
     * @description: 列表
     * @param json
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("getList")
    public Result getList(@RequestBody String json) {

        JSONObject jsonObject = JSON.parseObject(json);
//        if(TokenTool.getPermissionType().equals("t")) {
//            jsonObject.put("hszStudioIds",TokenTool.getHszStudioId());
//        }
        return ResultUtil.success(service.getListNew(jsonObject));
    }

    /**
     * @description: 详情
     * @param map
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("getById")
    public Result getById(@RequestBody Map<String,Object> map) {

        return ResultUtil.success(service.getById(map));
    }
    /**
     * @description: 获取介绍人详情
     * @param map
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("getByPid")
    public Result getByPid(@RequestBody Map<String,Object> map) {
        return ResultUtil.success(service.getByPid(map));
    }

    /**
     * @description: 详情
     * @param map
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("deleteSignature")
    public Result deleteSignature(@RequestBody Map<String,Object> map) {
        service.deleteSignature(map);
        return ResultUtil.success();
    }
    /**
     * @description: 修改用户头像
     * @param map
     * @return: com.example.utils.Result
     * @author: xxx
     * @time: 2022/6/20 15:35
     */
    @RequestMapping("updateImg")
    public Result updateImg(@RequestBody Map<String,Object> map) {
        if(TokenTool.getPermissionType().equals("m")) {
            String key = "user:img";
            redisUtil.set(key,map.get("url").toString());
            return ResultUtil.success();
        }else{
            return ResultUtil.error(-500,"无权限");
        }

    }
    /**
     * @description: 修改用户头像
     * @param map
     * @return: com.example.utils.Result
     * @author: xxx
     * @time: 2022/6/20 15:35
     */
    @RequestMapping("update")
    public Result update(@RequestBody Map<String,Object> map) {
        if(TokenTool.getPermissionType().equals("m")) {
            service.update(map);
            return ResultUtil.success();
        }else{
            return ResultUtil.error(-500,"无权限");
        }

    }
    /**
     * @description: 冻结
     * @param map
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("stateFrozen")
    public Result stateFrozen(@RequestBody Map<String,Object> map) {
        if(TokenTool.getPermissionType().equals("m")){
            map.put("state","PROHIBIT");
            service.update(map);
            return ResultUtil.success();
        }else{
            return ResultUtil.error(-500,"无权限");
        }
    }
    /**
     * @description: 解冻
     * @param map
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("updateState")
    @Transactional(rollbackFor = Exception.class)
    public Result updateState(@RequestBody Map<String,Object> map) {

        if(TokenTool.getPermissionType().equals("m")){
            map.put("state","NORMAL");
            service.update(map);
            return ResultUtil.success();
        }else{
            return ResultUtil.error(-500,"无权限");
        }
    }
    /**
     * @description: 设置vip
     * @param map
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("updateVip")
    public Result updateVip(@RequestBody Map<String,Object> map) {

        if(TokenTool.getPermissionType().equals("m")){
            List<String> userIds = Arrays.asList(map.get("ids").toString().split(","));
            service.updateVip(map.get("endTime").toString(),userIds);
            return ResultUtil.success();
        }else{
            return ResultUtil.error(-500,"无权限");
        }
    }

}
