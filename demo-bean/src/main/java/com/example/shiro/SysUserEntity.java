package com.example.shiro;


import lombok.Data;

import java.io.Serializable;

@Data
public class SysUserEntity implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 用户ID
     */
    private String id;
    /**
     * 用户名
     */
    private String name;
    private String nikeName;
    private String type;
    private String loginpass;
    private String createDate;

    /**
     * 密码
     */
    private String password;
    /**
     * 盐值
     */
    private String salt;
    /**
     * 状态:NORMAL正常  PROHIBIT禁用
     */
    private String state;


}
