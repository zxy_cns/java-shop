package com.example.service;

import com.alibaba.fastjson.JSONObject;
import com.example.mapper.BannerMapper;
import com.example.mapper.GetCreditsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @description: 管理积分获取记录
 * @author: xxx
 * @create: 2021/7/19 17:34
 */
@Service
public class GetCreditsService {
    @Autowired
    private GetCreditsMapper mapper;

    public List<Map<String, Object>> getList(JSONObject jsonObject) {

        return mapper.getList(jsonObject);
    }

    public Integer getCount(JSONObject jsonObject) {

        return mapper.getCount(jsonObject);
    }

    public Map<String, Object> getById(Map<String, Object> map) {
        return mapper.getById(map);
    }

    //  积分消耗记录添加
    public void saveExpend(Map<String, Object> map) {
        mapper.saveExpend(map);
    }

    // 上架商品成功添加积分记录
    public void saveAdd(Map<String, Object> map) {
        mapper.saveAdd(map);
    }

    // 邀请人消费成功 添加积分记录
    public void saveInviteAdd(Map<String, Object> map) {
        mapper.saveInviteAdd(map);
    }

    // 撤销兑换 返还积分
    public void saveRefund(Map<String, Object> map) {
        mapper.saveRefund(map);
    }

    public Map<String, Object> getMyCredits(Map<String, Object> map) {
        return mapper.getMyCredits(map);
    }
    // 获取我的数据（今天获得的积分、总共消费的积分、本月获得的积分）
    public Map<String, Object> getMyData(Map<String, Object> map) {
        return mapper.getMyData(map);
    }



    public void update(Map<String, Object> map) {
        mapper.update(map);
    }

    public void delete(Map<String, Object> map) {
        mapper.delete(map);
    }

}
