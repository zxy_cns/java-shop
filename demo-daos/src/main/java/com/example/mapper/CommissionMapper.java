package com.example.mapper;

import com.alibaba.fastjson.JSONObject;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @description: 佣金
 * @author: xxx
 * @create: 2021/7/19 17:24
 */
@Mapper
public interface CommissionMapper {

    List<Map<String, Object>> getList(@Param("jsonObject") JSONObject jsonObject);
    Integer getCount(@Param("jsonObject") JSONObject jsonObject);
    List<Map<String, Object>> getWithdrawList(@Param("jsonObject") JSONObject jsonObject);
    Integer getWithdrawCount(@Param("jsonObject") JSONObject jsonObject);
    Map<String, Object> getWithdrawById(Map<String, Object> map);
    Map<String, Object> getById(Map<String, Object> map);
    void save(Map<String, Object> map);
    void saveCommissionLog(Map<String, Object> map);
    void saveWithdraw(Map<String, Object> map);
    Integer update(Map<String, Object> map);
    Integer updateWithdraw(Map<String, Object> map);
    void delete(Map<String, Object> map);
}
