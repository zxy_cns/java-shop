package com.example.utils;


import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * 功能描述：日期处理的工具类
 *
 * @param * @param null
 * @author: xxx
 * @date 2020/11/24
 * @return
 */
public class DateUtil {
    /**
     * 默认日期格式
     */
    public static final String DEFAULT_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String DEFAULT_FORMAT_UUID = "yyyyMMddHHmmssSSSS";

    public static String getFormatData(String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.format(Calendar.getInstance().getTime());

    }
}
