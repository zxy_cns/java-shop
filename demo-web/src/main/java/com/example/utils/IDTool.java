package com.example.utils;

import java.util.UUID;

public class IDTool {

    public static String getUUID32() {

        return UUID.randomUUID().toString().replace("-", "");

    }

    public static long getIntId() {
        synchronized (IDTool.class) {
            try {
                Thread.sleep(2);
            } catch (InterruptedException e) {

            }
            return System.currentTimeMillis();
        }
    }
}
