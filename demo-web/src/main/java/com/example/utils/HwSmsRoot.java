package com.example.utils;

import lombok.Data;

import java.util.List;

/**
 * @description:
 * @author: xxx
 * @create: 2022/11/2 11:59
 */
@Data
public class HwSmsRoot {

//    private List<HwSmsResult> result;
    private String code;
    private String description;

}
