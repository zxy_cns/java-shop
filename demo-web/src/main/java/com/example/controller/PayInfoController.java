package com.example.controller;

import com.example.service.PayInfoService;
import com.example.service.UserService;
import com.example.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @description: 支付信息
 * @author: xxx
 * @create: 2021/7/19 09:30
 */
@RestController
@RequestMapping("payInfo")
public class PayInfoController {

    @Autowired
    private PayInfoService service;
    @Autowired
    private UserService userService;
    @Autowired
    private RedisUtil redisUtil;
    /**
     * @description: 详情
     * @param map
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("getPayInfo")
    public Result getById(@RequestBody Map<String,Object> map) {
        map.put("id", TokenTool.getUserId());
        return ResultUtil.success(service.getById(map));
    }
    /**
     * @description: 详情
     * @param map
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("getPayInfoById")
    public Result getPayInfoById(@RequestBody Map<String,Object> map) {
        return ResultUtil.success(service.getById(map));
    }
    /**
     * @description: 修改
     * @param map
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("update")
    public Result update(@RequestBody Map<String,Object> map) {
        //3.获取生成的验证码信息
        String verifyCode = map.get("verifyCode").toString();
        String uuid = map.get("uuid").toString();
        String name = map.get("name").toString();
        if (redisUtil.get("verifyCode:"+uuid) != null) {
            String key = "verifyCode:" + uuid;
            String verifyCode_redis = redisUtil.get(key).toString().toLowerCase();
            //由于redis存储了验证码信息,所以当跳转后在浏览器点返回时候,不改变验证码又可以登录了.所以要在获取后立即删除
            redisUtil.delete(key);
            if (verifyCode != null && verifyCode.toLowerCase().equals(verifyCode_redis)) {
                if (!TokenTool.getPermissionType().equals("t")){
//                    String code = map.get("code").toString();
//                    Map<String, String>  codeMap = redisUtil.getMap("code:"+name);
//                    if (codeMap != null && codeMap.get(name).equals(code)){
//                        redisUtil.delete("code:"+name);
//                    }else{
//                        return ResultUtil.error(500,"短信验证码错误！");
//                    }
                }
                map.put("id",TokenTool.getUserId());
                service.update(map);
                return ResultUtil.success();
            }else{
                return ResultUtil.error(500,"验证码错误");
            }
        }else{
            return ResultUtil.error(500,"验证码错误");
        }

    }
}
