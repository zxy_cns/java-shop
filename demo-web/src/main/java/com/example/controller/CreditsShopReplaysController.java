package com.example.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.example.service.CreditsShopReplaysService;
import com.example.utils.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description: 商品评价 -- 积分平台商品
 * @author: xxx
 * @create: 2021/7/19 09:30
 */
@RestController
@RequestMapping("creditsShopReplays")
public class CreditsShopReplaysController {

    @Autowired
    private CreditsShopReplaysService service;

    /**
     * @param json
     * @description: 分页
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/7/19 09:30
     */
    @RequestMapping("getPage")
    public Result getPage(@RequestBody String json) {
        JSONObject jsonObject = JSON.parseObject(json);
        Page.getPage(jsonObject);
        List<Map<String, Object>> list = service.getList(jsonObject);
        int total = service.getCount(jsonObject);
        // 查询当前商品评价的平均分
        Map<String, Object> avaPrams = new HashMap<>();
        avaPrams.put("shopId", jsonObject.get("shopId"));
        Map<String, Object> avager = service.getAvager(avaPrams);
        String Avanum = String.valueOf(avager.get("avager"));
        return ResultUtil.successPageAvager(list, total, Avanum);
    }

    /**
     * @param
     * @description: 列表
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("getList")
    public Result getList(@RequestBody String json) {
        JSONObject jsonObject = JSON.parseObject(json);
        return ResultUtil.success(service.getList(jsonObject));
    }

    /**
     * @param map
     * @description: 详情
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("getById")
    public Result getById(@RequestBody Map<String, Object> map) {

        return ResultUtil.success(service.getById(map));
    }

    /**
     * @param map
     * @description: 保存
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("save")
    public Result save(@RequestBody Map<String, Object> map) {

        map.put("id", IDTool.getUUID32());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        map.put("userId", TokenTool.getUserId());
        service.save(map);
        return ResultUtil.success();
    }

    /**
     * @param map
     * @description: 修改
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("update")
    public Result update(@RequestBody Map<String, Object> map) {
        service.update(map);
        return ResultUtil.success();
    }

    /**
     * @param map
     * @description: 删除
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("delete")
    public Result delete(@RequestBody Map<String, Object> map) {
        service.delete(map);
        return ResultUtil.success();
    }
}
