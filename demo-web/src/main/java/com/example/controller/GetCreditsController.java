package com.example.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.example.service.CreditsOrderService;
import com.example.service.GetCreditsService;
import com.example.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description: 管理积分获取的记录
 * @author: xxx
 * @create: 2021/7/19 09:30
 */
@RestController
@RequestMapping("getCredits")
public class GetCreditsController {

    @Autowired
    private GetCreditsService service;

    @Autowired
    private CreditsOrderService creditsOrderService;

    /**
     * @param json
     * @description: 分页
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/7/19 09:30
     */
    @RequestMapping("getPage")
    public Result getPage(@RequestBody String json) {
        JSONObject jsonObject = JSON.parseObject(json);
        jsonObject.put("userId", TokenTool.getUserId());
        Page.getPage(jsonObject);
        List<Map<String, Object>> list = service.getList(jsonObject);
        int total = service.getCount(jsonObject);

        return ResultUtil.successPage(list, total);
    }

    /**
     * @param json
     * @description: 列表
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("getList")
    public Result getList(@RequestBody String json) {

        JSONObject jsonObject = JSON.parseObject(json);
        return ResultUtil.success(service.getList(jsonObject));
    }

    /**
     * @param map
     * @description: 详情
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("getById")
    public Result getById(@RequestBody Map<String, Object> map) {

        return ResultUtil.success(service.getById(map));
    }

    /**
     * @param map
     * @description: 积分消耗---兑换商品时消耗积分
     * @return: com.example.common.entity.Result
     */
    @RequestMapping("saveExpend")
    public Result saveExpend(@RequestBody Map<String, Object> map) {

        map.put("id", IDTool.getUUID32());
        service.saveExpend(map);
        return ResultUtil.success();
    }


    /**
     * @param map
     * @description: 积分获取---上架商品成功添加积分记录
     * @return: com.example.common.entity.Result
     */
    @RequestMapping("saveAdd")
    public Result saveAdd(@RequestBody Map<String, Object> map) {

        map.put("id", IDTool.getUUID32());
        service.saveAdd(map);
        return ResultUtil.success();
    }

    /**
     * @param map
     * @description: 积分获取---邀请的人消费成功添加积分记录
     * @return: com.example.common.entity.Result
     */
    @RequestMapping("saveInviteAdd")
    public Result saveInviteAdd(@RequestBody Map<String, Object> map) {
        map.put("id", IDTool.getUUID32());
        service.saveInviteAdd(map);
        return ResultUtil.success();
    }

    /**
     * @param map
     * @description: 积分获取---撤销兑换返还积分
     * @return: com.example.common.entity.Result
     */
    @RequestMapping("saveRefund")
    public Result saveRefund(@RequestBody Map<String, Object> map) {
        map.put("id", IDTool.getUUID32());
        service.saveRefund(map);
        return ResultUtil.success();
    }


    /**
     * @param map
     * @description: 查询我的积分记录
     * @return: com.example.common.entity.Result
     */
    @RequestMapping("getMyCredits")
    public Result getMyCredits(@RequestBody Map<String, Object> map) {
        map.put("id", TokenTool.getUserId());
        return ResultUtil.success(service.getMyCredits(map));
    }

    /**
     * @param map
     * @description: 查询我的积分记录
     * @return: com.example.common.entity.Result
     */
    @RequestMapping("getMyData")
    public Result getMyData(@RequestBody Map<String, Object> map) {
        map.put("id", TokenTool.getUserId());
        return ResultUtil.success(service.getMyData(map));
    }

    /**
     * @param map
     * @description: 修改
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("update")
    public Result update(@RequestBody Map<String, Object> map) {
        service.update(map);
        return ResultUtil.success();
    }

    /**
     * @param map
     * @description: 删除
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("delete")
    public Result delete(@RequestBody Map<String, Object> map) {
        service.delete(map);
        return ResultUtil.success();
    }
}
