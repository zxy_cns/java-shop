package com.example.common;

import com.example.utils.RedisUtil;
import com.example.utils.TokenTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @description:
 * @author: xxx
 * @create: 2021/11/20 17:15
 */
//@Component
public class TestInterceptor implements HandlerInterceptor {
    @Autowired
    private RedisUtil redisUtil;
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
//        System.out.println("TimeInterceptor 进入 Controller 某个方法之前");
//        System.out.println("Controller Name:"+((HandlerMethod)handler).getBean().getClass().getName());
//        System.out.println("Controller Method Name:"+((HandlerMethod)handler).getMethod().getName());
//        request.setAttribute("startTime", new Date().getTime());
//        String key = "studioId:status:" + TokenTool.getStudioId();
        /**
         * boolean 值： 确定了拦截器其余两方法是否执行
         */
//        if(redisUtil.get(key) != null && redisUtil.get(key).equals("1")){
            return true;
//        }else{
//            //未授权，返回
//            request.setAttribute("code", 401);
//            request.getRequestDispatcher("/oauth/unauthTest").forward(request, response);
//            return false;
//        }


    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {
//        System.out.println("TimeInterceptor 运行 Controller 某个方法时，方法抛出异常将不进入此方法");
//        long start = (long) request.getAttribute("startTime");
//        System.out.println("TimeInterceptor 处理时长为："+ (new Date().getTime() - start));

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
//        System.out.println("TimeInterceptor 完成 Controller 某个方法");
//        long start = (long) request.getAttribute("startTime");
//        System.out.println("TimeInterceptor 处理时长为："+ (new Date().getTime() - start));

    }

}
